bootsettings
=========

This role sets secure filesystem settings on grub config file

Requirements
------------

None

Role Variables
--------------

enable_bootsettings: true - [controls if the module will run at all]

Dependencies
------------

None

License
-------

MIT

Author Information
------------------

signits@acme.dev